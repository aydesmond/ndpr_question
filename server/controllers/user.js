import jwt from "jsonwebtoken";
import bcrypt from "bcryptjs";
import User from "../model/User";
import dotenv from 'dotenv';
dotenv.config();
const secretkey = process.env.secretkey;

exports.get_user_register = (req, res, next) => {
    res.status(200).json({
        data: "Please sign up"
    });
};

exports.post_user_register = (req, res, next) => {
    const { email, password, confirmPassword}  = req.body;
    if (!email || !password || !confirmPassword)
        return res.status(400).json({ status: 400, error: "Bad request", message: "All fields are required" });
    if (password.length < 8) return res.status(400).json({ message: "Password is too short" });
    if (password !== confirmPassword) return res.status(400).json({ message: "Password does not match" });
    User.findOne({
        where: { email }
    })
        .then(user => {
            if (user)
                return res.status(400).json({ status: "failed", error: "Conflict", message: 'User already exists',
            user: {
                id: user.id,
                email: user.email
            } });
            let hashedPassword;
            try {
                const salt = bcrypt.genSaltSync(10);
                hashedPassword = bcrypt.hashSync(password, salt);
            } catch (error) {
                throw error;
            }
            User.create({
                email,
                password: hashedPassword
            })
                .then(user => {
                    jwt.sign({ userId: user.id }, secretkey, (err, token) => {
                        if(!err) 
                       return res.status(200).json({
                            token,
                            status: "success",
                            user: {
                                data: {
                                    id: user.id,
                                    email: user.email,
                                    isAdmin: user.isAdmin
                                }
                                }
                               
                        });
                        res.status(400).json({message: err})
                    })
                })
                .catch(err => next(err))
        }).catch(err => next(err))
};


