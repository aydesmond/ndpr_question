import Questionnaire from "../model/Questionnaire";


exports.get_questionnaire = (req, res, next) => {
    Questionnaire.findAll()
    .then(questionnaire => {
        if(questionnaire.length > 0) {
            res.status(200).json({
                questionnaire
            })

        }else {
            res.json({message: "No questionnaire available"})
        }
        
    }).catch(err => next(err))
};

exports.post_questionnaire = (req, res, next) => {
    const {question, optionA, optionB, optionC, optionD, answer}  = req.body;
   if(!question || !optionA || !optionB || !optionC || !optionD || !answer)
   return res.status(400).json({ status: 400, error: "Bad request", message: "All fields are required" });
   Questionnaire.create({
       question, optionA, optionB, optionC, optionD, answer
   })
   .then(questionnaire => {
       if(!questionnaire)
       return res.status(400).json({message: "error"})
       res.status(200).json({message:"success", questionnaire})
   })
};
