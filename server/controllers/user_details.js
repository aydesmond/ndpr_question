import jwt from "jsonwebtoken";
import User from "../model/User";
import dotenv from 'dotenv';
dotenv.config();
const secretkey = process.env.secretkey;

exports.getCurrentUser = (req, res, next) => {
    const token = req.header("Authorization") || req.headers["Authorization"] || req.body.token;
    if(!token){
        res.status(403).json({message: " Token not found" });
    }else {
       jwt.verify(token, secretkey, (err, decoded)=> {
            if(err){
                const error = new Error("Invalid Token");
                error.statusCode = 401;
                res.json({
                    status: "false",
                    data: error
                })
            }else {
                req.userData = decoded;
                const {userId} = req.userData;
               User.findAll({
                   where: {id: userId}
               }) .then(user => {

                if(user){
                    res.json({
                        data:{
                            id: user[0].id,
                            email: user[0].email,
                            isAdmin : user[0].isAdmin
                        }
                    })
        
                }else {
                    res.json({message: "No user"})
                }
                }) 
        
            }
       });
    }
   
   
    };
    