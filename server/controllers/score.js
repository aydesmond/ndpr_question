import Score from "../model/Score";
import User from "../model/User";



exports.get_score = (req, res, next) => {
                const {userId} = req.userData;
                User.findOne({
                    where: {
                        id:userId
                    }
                }).then(user => {
                    if(user){ 
                        Score.findOne({
                            where: {
                                UserId: userId
                            }
                        })
                        .then(score => {
                            
                            if(score !== null && score.length > 0 && score.length !== null) {
                                if(score.dataValues.UserId === userId)
                                return res.json({score}) 
                            }else {
                                return res.json({message: "No score yet"}) 
                            } 
                        }).catch(err => next(err))
                    }else {
                        res.json({message: "No user yet"})
                    }
                }) .catch(err => next(err))
      
};

exports.post_score = (req, res, next) => {
    const {score}  = req.body;
    const {userId} = req.userData;
    console.log(req)
    if(!score)
        return res.status(400).json({ status: 400, error: "Bad request", message: "All fields are required" });
                
        User.findAll({
                where: {id: userId}
        }).then(user => {
            if(user){ 
                Score.findOne({
                    where: {
                        UserId:userId
                    }
                }).then(score => {
                    if(score)
                    return res.json({message: "User already has a score"})
                    Score.create({
                        score, UserId: userId
                        })
                        .then(score => {
                            console.log(score)
                            if(score)
                            return res.status(200).json({message:"success", score})
                            res.status(400).json({message: "error"})
                        }).catch(err => next(err))
                }).catch(err => next(err))  
                }else {
                    res.json({message: "No user"})
                }
        }).catch(err => next(err)) 
        
   
};
