import jwt from "jsonwebtoken";
import bcrypt from "bcryptjs";
import User from "../model/User";
import dotenv from 'dotenv';
dotenv.config();
const secretkey = process.env.secretkey;

// Login a user
exports.get_login= (req, res, next) => {
    res.status(200).json({
        data: "Please login"
    });
};

exports.postLogin = (req, res, next) => {
	const { email, password } = req.body;
	if (!email || !password) 
		return res.status(400).json({ status: 400, error: "Bad request", message: "All fields are required" });
		User.findOne({
			where: { email }
		})
			.then(user => {
				if (!user) {
					return res.status(400).json({ message: "Bad request, User not found" });
				}
				bcrypt
					.compare(password, user.password)
					.then(match => {
						if (!match) {
							res.status(400).json({message: "Bad request, User not found"})
						} else {
							// Creates a jwt and stores the userId in the encoded token
							jwt.sign({userId: user.id},
								secretkey,
								(err, token) => {
									if (!err) {
										res.json({
											token,
											user: {
												data:{
													id: user.id,
													username: user.email,
													isAdmin: user.isAdmin
												}
												
											}
										});
									} else {
										next(err);
									}
								}
							);
						}
					})
					.catch(err => {
						next(err);
					});
			})
			.catch(err => {
				next(err);
			});
};
