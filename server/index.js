import express from "express";
import dotenv from "dotenv";
import http from 'http';
dotenv.config();

const app = express();
import db from "./config/database";
import cors from "cors";
const PORT = 8000 || process.env.PORT;

// ROUTES
import user_login from "./routes/auth"
import user_signup from "./routes/user";
import user_details from "./routes/user_details";
import score from "./routes/score";
import upload_questionnaire from "./routes/questionnaire";
// import 

// MIDDLEWARES
app.use(express.json());
// This middleware always runs for all request
// and this present setting allows and domain to
// access resources (our api) from our site.
app.use(cors());
app.use(express.urlencoded({ extended: false }));


// Public folder
app.use(express.static('/public'));

// ROUTES
app.use("/api/user/signup/", user_signup);
app.use("/api/user/login/", user_login);
app.use("/api/questionnaire", upload_questionnaire);
app.use("/api/user", user_details);
app.use("/api/user/score", score);

// Home page route
app.get('/', (req, res) => {
    res.status(200).json(
      {
          message: 'Welcome'      
      }
    );
  });


//  Handle invalid route 
app.use('*', (req, res) => {
        res.status(404).json({
          status: 'Wrong request',
          error:  'Route does not exist',
        });
});

// SERVER
const server = http.createServer(app);

// PORT
db
    .sync()
    .then(result => {        
        // this creates a http server and listens for incoming requests
        server.listen(PORT, () => {
            console.log(`LISTENING ON PORT ${PORT}`)
        });
    })
    .catch(err => console.log(err));

module.exports = app;
