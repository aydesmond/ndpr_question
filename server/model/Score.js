import Sequelize from "sequelize";
import db from "../config/database";


class Score extends Sequelize.Model {}
Score.init(
	{   
		score: {
			type: Sequelize.STRING,
			allowNull: false
		}
	},
	{ sequelize:db }
);


export default Score;
