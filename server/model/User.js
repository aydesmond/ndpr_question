import Sequelize from "sequelize";
import db from "../config/database";
import Score from "./Score";

class User extends Sequelize.Model {}
User.init(
	{
        email: {
			type: Sequelize.STRING,
			allowNull: false
        },
		password: {
			type: Sequelize.STRING,
			allowNull: false
		},
		isAdmin: {
			type:Sequelize.BOOLEAN,
			allowNull: false,
			defaultValue: false
		}
	},
	{ sequelize:db }
);
User.hasOne(Score, { foreignKey: { allowNull: false }, onDelete: 'CASCADE' })
export default User;
