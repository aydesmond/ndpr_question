import Sequelize from "sequelize";
import db from "../config/database";


class Questionnaire extends Sequelize.Model {}
Questionnaire.init(
	{
      question: {
		  type: Sequelize.STRING,
		  allowNull: false
	  },
	  optionA: {
		type: Sequelize.STRING,
		allowNull: false
	},
	optionB: {
		type: Sequelize.STRING,
		allowNull: false
	}, 
	optionC: {
		type: Sequelize.STRING,
		allowNull: false
	},
	optionD: {
		type: Sequelize.STRING,
		allowNull: false
	},
	answer: {
		type: Sequelize.STRING,
		allowNull: false
	}		
	},
	{ sequelize:db }
);


export default Questionnaire;
