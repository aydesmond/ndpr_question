// CHECK IF USER IS LOGGED IN
import dotenv from 'dotenv';
dotenv.config();
import JWT from "jsonwebtoken";
const secretkey = process.env.secretkey;

/**
 * Simple middleware to authenticate a request based
 * on a token.
 * Pass this middleware to any route that requires authentication
 */

exports.checkUser = (req, res, next) =>{
        // Get auth header value
        const token = req.header("Authorization") || req.headers["Authorization"] || req.body.token;
     console.log(token)
        if(!token){
            res.status(403).json({message: "Unauthorized, token not found" });
        }else {
           JWT.verify(token, secretkey, (err, decoded)=> {
                if(err){
                    const error = new Error("Invalid Token");
                    error.statusCode = 401;
                    res.json({
                        status: "false",
                        data: "Token incorrect"
                    })
                }else {
                    req.userData = decoded;
                    next();  // Success. Proceed to the next middleware
                }
           });
        }
    
;}
