import express from "express";
import {postLogin, get_login} from "../controllers/auth_user";
const route = express.Router();

route.post("/", postLogin);
route.get("/", get_login);

export default route;

