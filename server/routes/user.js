import express from "express";
import {post_user_register, get_user_register} from "../controllers/user";
const route = express.Router();

route.post("/", post_user_register);
route.get("/", get_user_register )

export default route;

