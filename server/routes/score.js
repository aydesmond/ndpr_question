import express from "express";
const route = express.Router();
import {post_score, get_score} from "../controllers/score";
import {checkUser} from "../middleware/authenticate";

route.get("/", checkUser, get_score);
route.post("/", checkUser, post_score);

export default route;