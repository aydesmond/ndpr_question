import express from "express";
import {getCurrentUser} from "../controllers/user_details";
const route = express.Router();

route.get("/", getCurrentUser)

export default route;

