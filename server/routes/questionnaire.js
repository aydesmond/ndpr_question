import express from "express";
const route = express.Router();
import {post_questionnaire, get_questionnaire} from "../controllers/questionnaire";
import {checkUser} from "../middleware/authenticate";

route.get("/", checkUser, get_questionnaire);
route.post("/", checkUser, post_questionnaire);

export default route;