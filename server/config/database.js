import Sequelize from "sequelize";
import dotenv from "dotenv";
dotenv.config();
// const db_password = process.env.password;
const db_user = process.env.user;
const db_host = process.env.host;
const db_database = process.env.database;


//Creates a Sequelize instance and sets the database config
const sequelize = new Sequelize(db_database, db_user, "", {
    dialect: "mysql",
    host: db_host
})


export default sequelize;