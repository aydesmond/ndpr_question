import React, {useEffect} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Provider} from 'react-redux';
import store from './store';
import Header from "./components/Header";
import Footer from "./components/Footer";
import "semantic-ui-css/semantic.min.css";
import {loadUser} from "./store/actions/authAction";
import './App.css';

const App =() => {
  useEffect(()=> {
    store.dispatch(loadUser());
  }) 
  return (
    <Provider store={store}>
    <div className="body">
    <div className="overlay">
      <Header />
      <Footer />
    </div>
    </div>
    </Provider>
  );
}

export default App;
