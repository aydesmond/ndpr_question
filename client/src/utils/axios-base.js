import axios from 'axios';

// Creates a base instance for all axios based request
const instance = axios.create({
    baseURL: 'http://localhost:8000'
});

export default instance;