import React, { Fragment } from "react";
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { logout } from '../store/actions/authAction';
import { clearErrors } from "../store/actions/errorAction";
import { NavLink} from 'reactstrap';

const Logout = (props) => {
    const onLogout = (e) => {
        props.logout();
    }

    return (
        <Fragment>
            <NavLink href="#" onClick={onLogout}>
                Logout
            </NavLink>
        </Fragment>
    )
}

Logout.propTypes = {
    logout: PropTypes.func.isRequired
};

export default connect(
    null,
    { logout, clearErrors }
)(Logout);