import React, { useEffect, useState } from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {Alert} from "reactstrap"
import PropTypes from 'prop-types';
import '../App.css';
import { Button, Form } from 'semantic-ui-react';
import { register } from '../store/actions/authAction';
import { clearErrors } from "../store/actions/errorAction";


const Register = (props) => {

  const [user, setUser] = useState({
    email: "",
    password: "",
    confirmPassword: ""
  });

  const [initialized, setInitialized] = useState(false);
  const [msg, setMsg] = useState('');
  const [errs, setErrs] = useState({
    email: "",
    password: "",
    confirmPassword: ""
  });


  useEffect(() => {
    const { auth } = props;
    if (!initialized) {
        if (auth.token !== null || auth.isAuthenticated) {
          if(props.location.pathname === "/register"){
                props.history.push("/");  
                setInitialized(true);          
          }else {
          props.history.push(props.location.pathname);
          setInitialized(true);
        }  
      }
        if(props.error.msg.message){
          setMsg(props.error.msg.message)
        }     
    }
  }, [props, initialized]);

  const change = e => {
    let { name, value } = e.target;
    let targets = { ...user, [name]: value };
    setUser(targets);
  }

  const submitForm = e => {
    e.preventDefault();
    if(!user.password && !user.email && !user.confirmPassword){
      setErrs({
        email: "Email Address required",
        password: "Password required",
        confirmPassword: "Confirm Password required",

      });
    }
    else if (!user.password ) {
      setErrs({
        email: "",
        password: "Password required",
        confirmPassword: "",

      });
    }else if(!user.email ){
      setErrs({
        email: "Email required",
        password: "",
        confirmPassword: ""
      });
    }else if(!user.confirmPassword){
      setErrs({
        email: "",
        password: "",
        confirmPassword: "Confirm Password required"
      });
    }
    else if (user.password.length < 8) {
      setErrs({
        password: "Minimum of 8 characters"
      });
    }
    else if (user.password !== user.confirmPassword) {
      setErrs({
        confirmPassword: "Password combinations do not match"
      });
    } else {
      setErrs({
        email: "",
        password: "",
        confirmPassword: ""
      });
      
    }
    props.register(user);
    props.clearErrors();
    setUser({
      email: "",
      password: "",
      confirmPassword: ""
    });
    

  }

  return (
<div id="container">
    <div className='reg-form'>
      
        <div className="form-wrapper" >
          <h1 className="signup-text"> Registration Form </h1>
          {msg ? <Alert color="danger">{msg}</Alert> : ""}

          <Form onSubmit={submitForm} style={{ 'margin': '0', 'padding': '0' }} method="POST">
            <Form.Field>
              <label className="important"> <span id="label">Email Address</span> 
                <input type="email" name="email" onChange={change} placeholder="Email Address" value={user.email} id="tags" />
                <p className="err-text">{errs.email}</p>
              </label>
            </Form.Field>
            <Form.Field>
              <label className="important"> <span id="label">Password</span> 
                <Form.Input type="password" name="password" onChange={change} placeholder="Password" value={user.password} id="tags" />
                <p className="err-text">{errs.password}</p>
              </label>
            </Form.Field>

            <Form.Field>
              <label className="important"><span id="label">Confirm Password</span> 
                <input type="password" name="confirmPassword" onChange={change} placeholder="Confirm Password" value={user.confirmPassword} id="tags" />
                <p className="err-text">{errs.confirmPassword}</p>
              </label>
            </Form.Field>

            <Button type="submit" className="btn" id="btnId"> Register </Button>

          </Form>
          <div className="extra ">
               Already a user? <NavLink to="/login" className="register"> Click here to login</NavLink>
            </div>
        </div>

        </div>

</div>

  )
}

register.propTypes = {
  error: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  register: PropTypes.func.isRequired,
  clearErrors: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  error: state.error,
  auth: state.auth
});


export default connect(
  mapStateToProps,
  { register, clearErrors }
)(withRouter(Register));