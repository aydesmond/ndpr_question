import React, { Fragment } from 'react';
import { Route, NavLink as RRNavLink, Switch, withRouter } from 'react-router-dom';
import Logout from "./Logout";
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { clearErrors } from "../store/actions/errorAction";
import Questionnaire from "./Questionnaire";
import Signup from "./Register";
import Home from "./Home";
import {
    Navbar,
    Nav,
    NavItem,
    NavLink,
    Container,
    Row,
    Col
} from 'reactstrap';
import Login from "./Login";
import PrivateRoute from "./PrivateRoute";

const Header = (props) => {

    const { isAuthenticated, user } = props.auth;

    // show different navbars based on authentication and user
    const authLinks = (
        <Fragment>
            <NavItem>
                <NavLink to="/questionnaire" tag={RRNavLink}>Assessment</NavLink>
            </NavItem>
            <NavItem>
                <Logout />
            </NavItem>
        </Fragment>
    );

    const guestLinks = (
        <Fragment>
            <NavItem>
                <NavLink to="/login" tag={RRNavLink}>Login</NavLink>
            </NavItem>
            <NavItem>
                <NavLink to="/register" tag={RRNavLink}>Sign-up</NavLink>
            </NavItem>
        </Fragment>
    );


    const adminLinks = (
        <Fragment>
            <NavItem>
                <NavLink to="/profile" tag={RRNavLink}>Profile</NavLink>
            </NavItem>
            <NavItem>
                <NavLink to="/scores" tag={RRNavLink}>Scores</NavLink>
            </NavItem>
            <NavItem>
                <NavLink to="/users" tag={RRNavLink}>Users</NavLink>
            </NavItem>
            <NavItem>
                <Logout />
            </NavItem>
        </Fragment>
    );

    const getNavSets = () => {
        if (!isAuthenticated) 
            return guestLinks

        return user.data.isAdmin ? adminLinks : authLinks;
    }

    return (
        <div >
            <div style={{ border: "1px solid black" }} className="navs">

                <Navbar sticky="top" color="fade" light expand="md" style={{ backgroundColor: 'white' }}>
                    <Container>
                        <Row noGutters className="position-relative w-100 align-items-center">
                            <Col md="12">

                                <Nav className="ml-auto " navbar>
                                    <NavItem className="navbar-link">
                                        <NavLink to="/" tag={RRNavLink}></NavLink>
                                    </NavItem>
                                    { getNavSets() }
                                </Nav>
                            </Col>
                        </Row>
                    </Container>
                </Navbar>
            </div>
            <Fragment>
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/register" component={Signup} />
                    <Route exact path="/login" component={Login} />
                    <PrivateRoute exact path="/questionnaire" component={Questionnaire} />
                </Switch>
            </Fragment>
        </div>

    );
}

Header.propTypes = {
    auth: PropTypes.object.isRequired,
    questionnaire: PropTypes.object.isRequired,
}

const mapStateToProps = state => ({
    auth: state.auth,
    questionnaire: state.questionnaire
});
export default connect(mapStateToProps,
    { clearErrors }
)(withRouter(Header));