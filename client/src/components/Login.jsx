import React, { useEffect, useState } from 'react';
import { withRouter, NavLink } from 'react-router-dom';
import {Alert} from "reactstrap"
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import '../App.css';
import { login } from '../store/actions/authAction';
import { clearErrors } from "../store/actions/errorAction";
import { Button, Form } from 'semantic-ui-react';

const Login = (props) => {
    const [loggedIn, setLoggedIn] = useState({
        email: "",
        password: ""
    });
    const [initialized, setInitialized] = useState(false);
    const [msg, setMsg] = useState('');
    const [errs, setErrs] = useState({
        email: "",
        password: ""
    
      });

    useEffect(() => {
        const { auth } = props;
        if (!initialized) {
            if (auth.token !== null || auth.isAuthenticated) {
                if(props.location.pathname === "/login"){
                    props.history.push("/");
                    setInitialized(true);
                }else {
                props.history.push(props.location.pathname);
                setInitialized(true);
                }
            }
            
            if (props.error.msg.message) {
                setMsg(props.error.msg.message); 
                
            }     
        }
    }, [props, initialized])

    const change = e => {
        const { name, value } = e.target;
        let x = { ...loggedIn, [name]: value }
        setLoggedIn(x)
    }

    const submitForm = e => {
        e.preventDefault();
        if(!loggedIn.password && !loggedIn.email){
            setErrs({
                email: "Email required",
                password: "Password required"
        
              });
        }
        else if (!loggedIn.password ) {
            setErrs({
              email: "",
              password: "Password required"
              
      
            });
        }else if(!loggedIn.email ){
            setErrs({
                email: "Email required",
                password: ""
              });
        }
        else {
            setErrs({
                email: "",
                password: ""
              }); 
        }
        props.login(loggedIn);
        props.clearErrors();
        setLoggedIn({
            email: "",
            password: ""
        });
        
    }

    return (
    <div id="container">
        <div className="reg-form">
            <div className="signin-cont">
                <div className="form-wrapper sign-in" >
                    <h1 className="signup-text">Login</h1>
                    {msg ? <Alert color="danger">{msg}</Alert> : ""}
                    <Form onSubmit={submitForm} action="POST" >
                      <Form.Field>
                      <label><span id="label">Email Address</span>
                            <input type="text" name="email" value={loggedIn.email} onChange={change} placeholder="Email Address" id="tags"/>
                            <p className="err-text">{errs.email}</p>
                        </label>
                      </Form.Field>
                        <Form.Field>
                        <label><span id="label">Password</span>
                            <input type="password" name="password" value={loggedIn.password} onChange={change} placeholder="Password" id="tags"/>
                            <p className="err-text">{errs.password}</p>
                        </label>
                        </Form.Field>
                       
                        <Button type="submit" className="submit" id="btnId">Submit</Button>
                    </Form>
                    <div className="extra "> New user? 
                <NavLink to="/register" className="register"> Click here to register</NavLink>
            </div>
                </div>
            </div>
           
        </div>
        </div>
    )
}

// PropTypes
login.propTypes = {
    error: PropTypes.object.isRequired,
    login: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    clearErrors: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
    error: state.error,
    auth: state.auth
});

export default connect(
    mapStateToProps,
    { login, clearErrors}
)(withRouter(Login));