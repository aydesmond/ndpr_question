import React, {useState, useEffect} from "react";
import {getQuestion} from "../store/actions/questionnaireAction";
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

const Home = (props) => {
    const [initialized, setInitialized] = useState(false);
    useEffect(() => {
        if(!initialized) {
            props.getQuestion();  
            setInitialized(true)  
        }
       
        
    }, [initialized]);

    return (
        <div style={{marginTop: "100px"}}> 
        
            <div className="test"> 
                <h2>NDPR Academy Assessment</h2>
            </div>
            <div>
                <p></p>
            </div>
        </div>
    )
};

Home.propTypes = {
    auth: PropTypes.object.isRequired,
    getQuestion: PropTypes.func.isRequired,
    questionnaire: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    auth: state.auth,
    questionnaire: state.questionnaire
});

export default connect(
    mapStateToProps,
    { getQuestion }
)(Home);
