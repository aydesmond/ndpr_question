import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Container, Col, Row, Form, Button} from 'reactstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { clearErrors } from "../store/actions/errorAction";
import { getQuestion, postScore } from "../store/actions/questionnaireAction";
import '../App.css';

const shuffle = (a) => {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
};


class Questions extends Component {
    shuffledQuiz = shuffle(this.props.questionnaire.questionnaire);
    state = {
        currentques: 0,
        answer: "",
        id: "",
        number: 0,
        userAnswer: null,
        submittedAns: [],
        question: "",
        options: {
            A: "",
            B: "",
            C: "",
        },
        count: 60,
        score: null
    };

    

    loadQuiz = () => {
        const questions = this.shuffledQuiz;
        const { currentques } = this.state;
        if(this.props.questionnaire.questionnaire.length <= 0){
            this.setState((state) => {
                return {
                    number: "",
                    id: "",
                    question: "",    
                    options: {
                        A: "",
                        B: "",
                        C: ""
                    }           
                }
            });
        }
        else {
            this.setState((state) => {
                return {
                    number: state.number + 1,
                    id: questions[currentques].id,
                    question: questions[currentques].question,    
                    options: {
                        A: questions[currentques].optionA,
                        B: questions[currentques].optionB,
                        C: questions[currentques].optionC
                    }           
                }
            });
        }
       
    };

    start = () => {
        this.myInterval = setInterval(() => {
            this.setState(prevState => ({
                count: prevState.count - 1
            }))
        }, 1000);
    }

    componentDidMount() {
        this.loadQuiz();
        this.start();
    };


    componentDidUpdate(prevProps, prevState) {
        const questions =  this.shuffledQuiz;
        const { currentques, number } = this.state;

        if (currentques !== prevState.currentques) {
            this.setState((state) => {
                return {
                    number: state.number + 1,
                    id: questions[currentques].id,
                    question: questions[currentques].question,
                    options: {
                        A: questions[currentques].optionA,
                        B: questions[currentques].optionB,
                        C: questions[currentques].optionC
                    }     

                }
            })
        }
    };

    checkAnswer = (answer) => { const { id, options } = this.state;
    var key = Object.keys(options).filter(function (key) { return options[key] === answer })[0]
        this.setState({
            answer,
            userAnswer: key,
            
        })
    };


    submitQuiz = () => {
        const questions = this.shuffledQuiz;;
        const { allAnswer, currentques, userAnswer, submittedAns, score, number} = this.state;
        
        console.log(userAnswer, questions[currentques].answer)
        if( userAnswer === questions[currentques].answer){
            submittedAns.push(userAnswer)
            const result = submittedAns.length;
            this.setState({
                submittedAns: [...submittedAns],
                score: result * 2
            });

        }
        this.setState((state) => ({
            currentques: state.currentques + 1,
            userAnswer: null,
            question: questions[currentques].question,

        }));

        if(number === 6){
            console.log(score)
            this.props.postScore(score, this.props.auth.user.data.id,this.props.auth.token)
        }
    };


    clear = () => {
        const { count, submission} = this.state;
        const all = {
            competitionId: 1,
            submissions: submission
        }
        if (count === 0) {
            clearInterval(this.myInterval);
            console.log(this.props.auth.user)
            this.props.postQuizFinalScore(all, this.props.auth.user.data.id, this.props.auth.token)
            this.props.history.push("/tquiz/score")
        }
    };

    render() {
        const { question, id, userAnswer, count, options, updateAns, number, score } = this.state;
        return (
            <Container>


                <div className="user-table">

                    <Row>
                        <Col sm="10">
                            <div className="cards"> 
                            {this.props.questionnaire.questionnaire.length <= 0 ? 
                            <h1 style={{"textAlign":"center"}}>No questions Available</h1>: 
                             <Form>
                             <div key={id}>
                                 <p>{number}. {question}</p>
                                 <p size="md" color="info" className={`options ${userAnswer === options.A ? "selected" : null}`} onClick={() => this.checkAnswer(options.A)}  >{options.A}</p>
                                 <p size="md" color="info" className={`options ${userAnswer === options.B ? "selected" : null}`} onClick={() => this.checkAnswer(options.B)}  > {options.B}</p>
                                 <p size="md" color="info" className={`options ${userAnswer === options.C ? "selected" : null}`} onClick={() => this.checkAnswer(options.C)}  >{options.C}</p>
                                 <hr />
                                 <Button type="submit" color="success" size="sm" block onClick={this.submitQuiz} disabled={!userAnswer} style={{marginTop: "50px"}}>Submit</Button>
                             </div>
                         </Form>
                        }
                                {/* <p className="timer" onChange={this.clear()}>Timer  Left {min}:{sec} </p> */}
                               
                            </div>
                        </Col>
                    </Row>
                </div>
            </Container>
        )
    }
};

Questions.propTypes = {
    auth: PropTypes.object.isRequired,
    clearErrors: PropTypes.func.isRequired,
    getQuestion: PropTypes.func.isRequired,
    questionnaire: PropTypes.object.isRequired,
    postScore: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    questionnaire: state.questionnaire
});

export default connect(
    mapStateToProps,
    { getQuestion, clearErrors, postScore }
)(withRouter(Questions));