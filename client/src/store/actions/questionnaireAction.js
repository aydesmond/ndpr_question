import { GET_QUESTIONS, QUESTION_LOADING, FINAL_SCORE} from "./types";
import axios from '../../utils/axios-base';
import { returnErrorrs } from './errorAction';

export const getQuestion = () => (dispatch, getState) => {
    dispatch(setQuestionLoading());
    axios.get("/api/questionnaire", tokenConfig(getState))
        .then(res => {   
            dispatch({
                type: GET_QUESTIONS,
                payload: res.data
            });
            
        })
        .catch(err => dispatch(returnErrorrs(err.response.data, err.response.status)));
};


export const setQuestionLoading = () => {
    return {
        type: QUESTION_LOADING
    }
};

export const postScore = ( score, id, token) => dispatch => {
    console.log(score, id, token)
    axios.post("/api/user/score", score, id, {
        headers: {
            "Authorization": `${token}`
        }
        })
        .then(res => {
            dispatch({
                type: FINAL_SCORE,
                payload: res.data
            });
            
        })
        .catch(err => dispatch(returnErrorrs(err.response.data, err.response.status)));
};


// Setup config/headers and token
export const tokenConfig = getState => {
    // Get token from local storege
    const token = getState().auth.token;

    // Headers
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };

    if (token) {
        config.headers['Authorization'] = `${token}`;

    }
    return config;
};