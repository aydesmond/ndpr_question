import axios from '../../utils/axios-base';
import { returnErrorrs } from './errorAction';
import {
    USER_LOADING,
    USER_LOADED,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOGOUT_SUCCESS,
    AUTH_ERROR,
    REG_FAIL,
    REG_SUCCESS
} from './types';

// Check token and load user
export const loadUser = () => (dispatch, getState) => {
    // User loading
    dispatch({
        type: USER_LOADING
    });

    axios.get("/api/user", tokenConfig(getState))
        .then(res => dispatch({
            type: USER_LOADED,
            payload: res.data
        })
    )
        .catch(err => {
            dispatch(returnErrorrs(err.response.data, err.response.status));
            dispatch({
                type: AUTH_ERROR
            });
        });
}

// Register User
export const register = (userData) => (dispatch) => {
    // Headers
    const config = {
        headers : {
            'Content-Type' : 'application/json'
        }
    }

    axios.post('/api/user/signup/', userData, config)
        .then(res => {
            dispatch({
                type: REG_SUCCESS,
                payload: res.data
            });
            dispatch(loadUser());
        })
        .catch(err => {
            dispatch(returnErrorrs(err.response.data, err.response.status, 'REG_FAIL'));
            dispatch({
                type: REG_FAIL
            });
        });

}

// LOGIN USER
export const login = userData => dispatch => {
    // Headers
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }

    axios.post('/api/user/login/', userData, config)
        .then(res => {dispatch({
            type: LOGIN_SUCCESS,
            payload: res.data

        })
        dispatch(loadUser())
    })
        .catch(err => {
            dispatch(returnErrorrs(err.response.data, err.response.status, 'LOGIN_FAIL'));
            dispatch({
                type: LOGIN_FAIL
            });
        });
}


// Logout User
export const logout = () => {
    return {
        type: LOGOUT_SUCCESS
    };
};

// Setup config/headers and token
export const tokenConfig = getState => {
    // Get token from local storage
    const token = getState().auth.token;

    // Headers
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };

    if (token) {
        config.headers['Authorization'] = `${token}`;
    }
    return config;
}