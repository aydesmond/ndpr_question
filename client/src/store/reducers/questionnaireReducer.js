import { GET_QUESTIONS, QUESTION_LOADING, QUESTION_SCORE, FINAL_SCORE} from "../actions/types";

const initialState = {
    questionnaire: [],  
    loading: false,
    score: null,
    finalScore: null
}

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_QUESTIONS:
            return {
                ...state,
                ...action.payload,
                loading: false
            }
        case QUESTION_LOADING:
            return {
                ...state,
                loading: true
            }
        case QUESTION_SCORE:
            return {
                ...state,
                score: action.payload
            }
        case FINAL_SCORE:
            return {
                ...state,
                finalScore: action.payload
            }
       
        default:
            return state;
    }
}