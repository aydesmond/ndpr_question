import {combineReducers} from 'redux';
import auth from './authReducer';
import error from './errorReducer';
import questionnaire from "./questionnaireReducer"

export default combineReducers({
    auth,
    error,
    questionnaire
});